# Open Firewall port 1080 for TCP inbound traffic
New-NetFirewallRule -DisplayName "VPN_1080" -Direction Inbound -LocalPort 1080 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "VPN_1080" -Direction Outbound -LocalPort 1080 -Protocol TCP -Action Allow


New-NetFirewallRule -DisplayName "aria2" -Direction Inbound -LocalPort 6800 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "aria2" -Direction Outbound -LocalPort 6800 -Protocol TCP -Action Allow
