; 切换当前窗口置顶状态并显示红边
; 快捷键：Alt + Z

!z::  ; Alt + Z 快捷键
{
    ; 获取当前活动窗口的句柄
    activeHwnd := WinGetID("A")
    ; 检查窗口是否已经置顶
    isTopmost := WinGetExStyle(activeHwnd) & 0x8  ; 0x8 是 WS_EX_TOPMOST 标志

    ; 切换置顶状态
    if isTopmost {
        ; 取消置顶
        WinSetAlwaysOnTop(false, activeHwnd)
        ToolTip("取消置顶")
        ; 移除红边
        ; DestroyRedBorder()
    } else {
        ; 设置为置顶
        WinSetAlwaysOnTop(true, activeHwnd)
        ToolTip("窗口置顶")
        ; 显示红边
        ; ShowRedBorder(activeHwnd)
    }
    ; 显示提示信息，1 秒后消失
    SetTimer(() => ToolTip(), -1000)
}

; ; 显示红边
; ShowRedBorder(hwnd) {
;     ; 获取窗口的位置和大小
;     WinGetPos(&x, &y, &w, &h, hwnd)

;     ; 创建 GUI 作为红边
;     MyGui := Gui("+ToolWindow -Caption +AlwaysOnTop +E0x20")
;     MyGui.BackColor := "Red"
;     MyGui.MarginX := 0
;     MyGui.MarginY := 0
;     MyGui.SetFont("s1")  ; 设置边框宽度为 1 像素

;     ; 设置 GUI 的位置和大小
;     MyGui.Show("x" x " y" y " w" w " h" h " NoActivate")

;     ; 将 GUI 附加到目标窗口
;     WinSetTransparent(255, MyGui.Hwnd)
;     WinSetStyle("-0xC00000", MyGui.Hwnd)  ; 移除标题栏
;     WinSetExStyle("+0x80", MyGui.Hwnd)    ; 设置 WS_EX_LAYERED
; }

; ; 移除红边
; DestroyRedBorder() {
;     ; 查找并关闭所有红色边框的 GUI
;     guiList := WinGetList(, "AutoHotkeyGui")
;     for hwnd in guiList {
;         if WinGetTitle(hwnd) = "" {  ; 无标题的 GUI
;             WinClose(hwnd)
;         }
;     }
; }
