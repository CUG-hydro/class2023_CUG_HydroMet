' ToggleTopmost.vbs
' 切换当前窗口的置顶状态
Set objShell = CreateObject("WScript.Shell")

' 获取当前活动窗口的句柄
hwnd = objShell.AppActivate("")

If hwnd Then
    ' 获取窗口的当前状态
    Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
    Set colItems = objWMIService.ExecQuery("Select * From Win32_Process Where Handle = '" & hwnd & "'")
    
    For Each objItem in colItems
        ' 获取窗口的句柄
        hwnd = objItem.Handle
        Exit For
    Next

    ' 切换窗口的置顶状态
    Set objShell = Nothing
    Set objWMIService = Nothing

    Const SWP_NOMOVE = 2
    Const SWP_NOSIZE = 1
    Const HWND_TOPMOST = -1
    Const HWND_NOTOPMOST = -2

    ' 获取窗口的当前置顶状态
    Set objShell = CreateObject("Shell.Application")
    Set objWindow = objShell.Windows().Item(0)
    isTopmost = (objWindow.Topmost = True)

    ' 切换置顶状态
    If isTopmost Then
        objWindow.Topmost = False
    Else
        objWindow.Topmost = True
    End If
End If
